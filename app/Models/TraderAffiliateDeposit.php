<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TraderAffiliateDeposit extends Model
{
    use HasFactory;

    protected $dates = ['registrationDate', 'firstDepositDate'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'trader_affiliate_deposits';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trader()
    {
        return $this->belongsTo('App\Models\Trader', 'trader_id');
    }


}
