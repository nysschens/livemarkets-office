<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * User Provider Model.
 */
class TraderProvider extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'trader_providers';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Get the trader of a TraderProvider.
     */
    public function trader()
    {
        return $this->belongsTo('App\Models\Trader');
    }
}
