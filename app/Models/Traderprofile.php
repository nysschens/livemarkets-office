<?php

namespace App\Models;

class Traderprofile extends BaseModel
{
    protected $dates = [
        'dob',
        'created_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function trader()
    {
        return $this->belongsTo('App\Models\Trader');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Trader', 'created_by', 'trader_id');
    }
}
