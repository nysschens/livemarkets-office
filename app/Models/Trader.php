<?php

namespace App\Models;

use App\Models\Traits\HasHashedMediaTrait;
use App\Models\Presenters\TraderPresenter;
use DB;
use App\Models\Traderprofile;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;

class Trader extends Model implements HasMedia
{
    use HasFactory;
    use SoftDeletes;
    use TraderPresenter;
    use HasHashedMediaTrait;

    protected $guarded = [
        'id',
        'user_id',
        'userID',
        'created_by',
    ];

    protected $dates = [
        'deleted_at',
        'date_of_birth',
        'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @var mixed
     */
    private $data;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function providers()
    {
        return $this->hasMany('App\Models\TraderProvider');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profile()
    {
        return $this->hasOne('App\Models\Traderprofile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function traderprofile()
    {
        return $this->hasOne('App\Models\Traderprofile');
    }

    /**
     * Get the list of Traders related to the current User.
     *
     * @return array [array] roles
     */
    public function getRolesListAttribute()
    {
        return array_map('intval', $this->roles->pluck('id')->toArray());
    }

    /**
     * @return mixed
     */
    public function getTableColumns()
    {
        $table_info_columns = DB::select(DB::raw('SHOW COLUMNS FROM '.$this->getTable()));

        return $table_info_columns;
    }

    /**
     * @return mixed
     */
    public function createdBy()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id');
    }
}
