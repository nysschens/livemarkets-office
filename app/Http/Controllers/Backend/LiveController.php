<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\TraderAffiliateDeposit;
use Carbon\Carbon;
use DataTables;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Laracasts\Flash\Flash;

class LiveController extends Controller
{
    public function __construct()
    {
        // Page Title
        $this->module_title = 'Live Traders';

        // module name
        $this->module_name = 'live';

        // directory path of the module
        $this->module_path = 'live';

        // module icon
        $this->module_icon = 'c-icon cil-people';

        // module model name, path
        $this->module_model = "App\Models\TraderAffiliateDeposit";
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index()
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'Live';
        $page_heading = ucfirst($module_title);
        $title = $page_heading.' '.ucfirst($module_action);
        $$module_name = $module_model::paginate();

        //$$module_name_singular = $module_model::findOrFail($id);
        //$traders = Trader::where('id', $$module_name_singular->id)->first();

        /*
        $this->session = $LiveServiceApi->getAccessToken();
        if (!empty($this->session))
            Log::info("Session:  '$this->session' viewed by User:".auth()->user()->name.'(ID:'.auth()->user()->id.')');
            Flash::success("<i class='fas fa-check'></i> Live '".Str::singular($module_title)."' Session")->important();
        */

        $traders['id'] = 12345;

        return view(
            "backend.$module_path.index_datatable",
            compact('module_title', 'module_name', "$module_name", 'module_path', 'module_icon', 'module_action', "$module_name_singular", 'page_heading', 'title', 'traders')
        );
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function index_data()
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_session = $this->session;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'Live';

        //$jsonData = json_encode($sessionID, JSON_PRETTY_PRINT);
        //Log::info("Live data:  '$jsonData' viewed by User:".auth()->user()->name.'(ID:'.auth()->user()->id.')');

        $$module_name = $this->fetchFirstDeposits();
        return Datatables::of($$module_name)
            ->addColumn('checkbox', function ($data) {
                $module_name = $this->module_name;
                return view('backend.includes.live_selected', compact('module_name', 'data'));
            })
            ->addColumn('action', function($data) {
                $module_name = $this->module_name;
                return view('backend.includes.live_actions', compact('module_name', 'data'));
            })
            ->orderColumns(['userID'], '-:column $1')
            ->make(true);
    }

    /**
     * Select Options for Select 2 Request / Response.
     *
     * @return Response
     */
    public function index_list(Request $request)
    {
        $module_title = $this->module_title;
        $module_name = $this->module_name;
        $module_path = $this->module_path;
        $module_icon = $this->module_icon;
        $module_model = $this->module_model;
        $module_name_singular = Str::singular($module_name);

        $module_action = 'List';

        $page_heading = label_case($module_title);
        $title = $page_heading.' '.label_case($module_action);

        $term = trim($request->query());
        if (empty($term)) {
            return response()->json([]);
        }

        $query_data = $module_model::where('name', 'LIKE', "%$term%")->orWhere('email', 'LIKE', "%$term%")->limit(10)->get();

        $module_name = [];
        foreach ($query_data as $row) {
            $module_name[] = [
                'userID'   => $row->userID,
                'firstName' => $row->firstName.' (Nickname: '.$row->affNickname.')',
            ];
        }

        return response()->json($module_name);
    }

    /**
     * @return TraderAffiliateDeposit
     */
    public function fetchFirstDeposits()
    {
        $response = Http::get('https://admin-api.tradesmarter.com/crm/rest/affiliate-deposits', [
            'apiKey' => '2b0de9c1a8ae8a0bcea2e25b67d9bb90',
            'limit' => 10,
        ]);
        $traders = json_decode($response->body());
        foreach($traders as $trader){
            $model = new TraderAffiliateDeposit();
            $model->id = $trader->data->userID;
            $model->userID = $trader->data->userID;
            $model->token = $trader->data->token;
            $model->tester = $trader->data->tester;
            $model->country = $trader->data->country;
            $model->language = $trader->data->language;
            $model->registrationDate = $trader->data->registrationDate;
            $model->landingParams = $trader->data->landingParams;
            $model->firstDepositDate = $trader->data->firstDepositDate;
            $model->firstDepositAmount = $trader->data->firstDepositAmount;
            $model->save();
        }
        return "DONE";
    }

    /**
     * Return
     * @return type
     * @throws \Exception
     */
    public function getWallets()
    {
        $items = new TraderAffiliateDeposit();
        $items = $items->all();
        return Datatables::of($items)->addColumn('checkbox', function ($item) {
            return '<input type="checkbox" id="'.$item->id.'" name="update_wallet" />';
        })->make(true);
    }

}
