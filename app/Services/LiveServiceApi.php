<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Client;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

class LiveServiceApi
{
    protected $url;
    protected $http;
    protected $headers;

    /**
     * Pass in the model if we wanna
     * @var $model
     */
    protected $model;

    protected $clientId;
    protected $clientEmail;
    protected $clientSecret;
    protected $clientKey;
    protected $getTokenUri;

    protected $client;

    public $accessToken;

    /**
     * PlatformAPI constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->clientId = env('API_EMAIL', null);
        $this->clientSecret = env('API_SECRET', null);
        $this->getTokenUri = 'https://admin-api.tradesmarter.com';
        //$this->accessToken = $this->client->getToken($this->clientId, $this->clientSecret, $this->getTokenUri);
    }

    /**
     * @param string|null $uri
     * @param array $post_params
     * @return object|null
     */
    private function getResponse(string $uri = null, array $post_params = [])
    {
        $full_path = $this->url;
        $full_path .= $uri;

        $request = $this->http->get($full_path, [
            'headers'         => $this->headers,
            'timeout'         => 30,
            'connect_timeout' => true,
            'http_errors'     => true,
        ]);

        $response = $request ? $request->getBody()->getContents() : null;
        $status = $request ? $request->getStatusCode() : 500;

        if ($response && $status === 200 && $response !== 'null') {
            return (object) json_decode($response);
        }

        return null;
    }

    /**
     * @param string|null $uri
     * @param array $post_params
     * @return object|null
     */
    private function postResponse(string $uri = null, array $post_params = [])
    {
        $full_path = $this->url;
        $full_path .= $uri;

        $request = $this->http->post($full_path, [
            'headers'         => $this->headers,
            'timeout'         => 30,
            'connect_timeout' => true,
            'http_errors'     => true,
            'form_params'     => $post_params,
        ]);

        $response = $request ? $request->getBody()->getContents() : null;
        $status = $request ? $request->getStatusCode() : 500;

        if ($response && $status === 200 && $response !== 'null') {
            return (object) json_decode($response);
        }

        return null;
    }

    /**
     * @return object|null
     */
    public function getAccessToken()
    {
        $request = $this->client->request('POST', 'https://platform-api.tradesmarter.com/user/login', [
            'auth' => ['admin@livemarkets.co.za', '2b0de9c1a8ae8a0bcea2e25b67d9bb90']
        ]);

        $response = $request ? $request->getBody()->getContents() : null;
        $status = $request ? $request->getStatusCode() : 500;

        if ($response && $status === 200 && $response !== 'null') {
            return (object) json_decode($response);
        }
        return null;
    }

    /**
     * @return object|null
     */
    public function getLivetraders()
    {
        return $this->getResponse('getLivetraders');
    }

}
