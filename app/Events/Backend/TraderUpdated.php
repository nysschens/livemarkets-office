<?php

namespace App\Events\Backend;

use App\Models\Trader;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TraderUpdated
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public Trader $trader;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Trader $trader)
    {
        $this->trader = $trader;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
