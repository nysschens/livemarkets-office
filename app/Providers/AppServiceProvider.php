<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        Schema::defaultStringLength(191);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        Blade::component('components.backend-breadcrumbs', 'backendBreadcrumbs');

        /*
         * @see https://gist.github.com/bbengamin/487b65c85f86742e4f8ccf04e3250c43
         * @TODO    Override sendSwiftMessage() with custom template
         *
        $this->app->singleton('customMailer', function($app) {
            return new CustomMailer(
                $app['view'], $app['swift.mailer'], $app['events']
            );
        });
        */

    }
}
