<?php

namespace App\Providers;

use GuzzleHttp\Client;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Illuminate\Support\ServiceProvider;

class GuzzleServiceProvider extends ServiceProvider
{
    protected $app;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        \App::make('App\Services\LiveServiceApi');
        $this->app->bind('GuzzleHttp\Client\Client', function() {
            return new Client;
        });
    }

}
