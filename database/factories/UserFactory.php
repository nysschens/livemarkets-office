<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $first_name = $this->faker->firstName;
        $last_name = $this->faker->lastName;
        $name = $first_name.' '.$last_name;

        return [
            'name'                  => $name,
            'first_name'            => $first_name,
            'last_name'             => $last_name,
            'email'                 => $this->faker->unique()->safeEmail,
            'email_verified_at'     => now(),
            'password'              => Hash::make('000000'),
            'remember_token'        => Str::random(10),
            'mobile'                => $this->faker->phoneNumber,
            'date_of_birth'         => $this->faker->date,
            'avatar'                => 'img/default-avatar.jpg',
            'gender'                => 'Male',
            'created_at'            => now(),
            'updated_at'            => now(),
        ];
    }
}
