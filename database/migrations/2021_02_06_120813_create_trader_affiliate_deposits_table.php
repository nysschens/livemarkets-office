<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTraderAffiliateDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trader_affiliate_deposits', function (Blueprint $table) {
            $table->bigIncrements('id')->startingValue(1);
            $table->foreignId('trader_id');
            $table->string('userID');
            $table->string('token')->nullable();
            $table->string('country')->nullable();
            $table->string('language')->nullable();
            $table->timestamp('registrationDate')->nullable();
            $table->string('landingParams')->nullable();
            $table->timestamp('firstDepositDate')->nullable();
            $table->string('firstDepositAmount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trader_affiliate_deposits');
    }
}
