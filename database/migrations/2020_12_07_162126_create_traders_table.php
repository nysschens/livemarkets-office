<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traders', function (Blueprint $table) {
            $table->bigIncrements('id')->startingValue(1);
            $table->foreignId('user_id');
            $table->string('userID');
            $table->string('language');
            $table->string('country');
            $table->date('registrationDate')->nullable();
            $table->date('firstDepositDate')->nullable();
            $table->string('firstName');
            $table->string('lastName');
            $table->string('initials');
            $table->string('tester');
            $table->boolean('blocked');
            $table->boolean('frozen');
            $table->string('affiliateID');
            $table->string('accountLevel');
            $table->string('subGroup');
            $table->string('currency');
            $table->boolean('isRedepositor');
            $table->boolean('isDepositor');
            $table->string('pushwooshDeviceToken');
            $table->integer('adminID');
            $table->string('saleStatus');
            $table->string('documentsRequested');
            $table->string('documentsVerified');
            $table->string('lastMessageDate');
            $table->string('lastMessage');
            $table->string('crmUser');
            $table->string('landingParams');
            $table->string('userTags');
            $table->string('registrationIP');
            $table->string('affNickname');
            $table->string('parentAffToken');

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->integer('deleted_by')->unsigned()->nullable();
            $table->timestamps(0);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traders');
    }
}
