<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTraderprofilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traderprofiles', function (Blueprint $table) {
            $table->bigIncrements('id')->startingValue(1);
            $table->foreignId('trader_id');
            $table->string('userID');
            $table->string('email')->nullable();
            $table->string('firstName');
            $table->string('lastName');
            $table->date('dob');
            $table->integer('mobile');
            $table->integer('mobile2');
            $table->integer('mobile2Verified');
            $table->string('qq');
            $table->string('location');
            $table->string('locale');
            $table->string('nationalID');
            $table->string('confirmed');
            $table->boolean('validated');
            $table->string('frozen');
            $table->string('blocked');
            $table->string('affiliateID');
            $table->string('tester');
            $table->string('facebookID');
            $table->string('vkID');
            $table->string('social');
            $table->string('nickname');
            $table->integer('accountLevel');
            $table->string('userCurrency');
            $table->string('depositCount');
            $table->string('practiceMode');
            $table->string('siteID');
            $table->string('questionaryDone');
            $table->string('pushwooshDeviceToken');
            $table->string('globalConsent');
            $table->string('fxRiskFactor');
            $table->string('questionaryRequired');
            $table->string('questionaryEnabled');
            $table->string('showQuestionary');
            $table->string('welcomeEmailSent');
            $table->string('payoutDeltas');
            $table->string('questionaryRandom');
            $table->string('practiceExpired');
            $table->string('expirationDate');
            $table->string('nextResetDate');
            $table->boolean('practiceWalletExists');
            $table->string('address1');
            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('country');
            $table->string('userClass');
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->integer('deleted_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traderprofiles');
    }
}
