@extends ('backend.layouts.app')

@section('title') {{ $module_action }} {{ $module_title }} @endsection

@section('breadcrumbs')
<x-backend-breadcrumbs>
    <x-backend-breadcrumb-item type="active" icon='{{ $module_icon }}'>{{ $module_title }}</x-backend-breadcrumb-item>
</x-backend-breadcrumbs>
@endsection

@section('content')
<div class="card">
    <div class="card-body">

        <div class="row">
            <div class="col">
                <h4 class="card-title mb-0">
                    <i class="{{ $module_icon }}"></i> {{ $module_title }} <small class="text-muted">{{ $module_action }}</small>
                </h4>
                <div class="small text-muted">
                    {{ __('labels.backend.traders.index.sub-title') }}
                </div>
            </div>
            <div class="col">
                <div class="form-group float-right">
                    <label class="c-switch c-switch-lg c-switch-label c-switch-danger">
                        <input type="checkbox" class="c-switch-input" id="live_id">
                        <span class="c-switch-slider" data-checked="Live" data-unchecked="Off"></span>
                    </label>
                </div>
            </div>
        </div>
        <div class="small text-muted">
            For a selective result, search by Group or CRM item.
        </div>

        <div class="col">
        {{ html()->modelForm($traders, 'GET', route('backend.live.index'))->attributes(['enctype'=>"multipart/form-data"])->open() }}
            <div class="row">

                <!--// Select subgroup -->
                <div class="col-md-3">
                    <div class="float-left">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="search_id">Search</label>
                            </div>
                            <select class="custom-select" id="search_id">
                                <option selected>select group</option>
                                <option value="0"> Online </option>
                                <option value="1"> Online Advanced </option>
                                <option value="2"> All Users </option>
                                <option value="3"> Real Money </option>
                                <option value="4"> Play Money </option>
                                <option value="5"> Confirmed </option>
                                <option value="6"> Non Confirmed </option>
                                <option value="7"> Deposited Once </option>
                                <option value="8"> Deposited (multiple) </option>
                                <option value="9"> Have never deposited </option>
                                <option value="10"> Failed Validation </option>
                                <option value="11"> Testers </option>
                            </select>
                        </div>
                    </div>
                </div>

                <!--// Select subgroup -->
                <div class="col-md-2">
                    <div class="float-left">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="crm_id">CRM</label>
                            </div>
                            <select class="custom-select" id="crm_id">
                                <option selected>select item</option>
                                <option value="0">All Leads</option>
                                <option value="1">MT4/5 All Leads</option>
                                <option value="2">FX/CFD All Leads</option>
                                <option value="3">My Leads</option>
                                <option value="4">FX/CFD My Leads</option>
                                <option value="5">MT4/5 My Leads</option>
                                <option value="6">My Clients</option>
                                <option value="7">My Deposits</option>
                                <option value="8">My Withdrawals</option>
                                <option value="9">Pending Files</option>
                                <option value="10">Bad Files</option>
                                <option value="11">No Files</option>
                                <option value="12">Log</option>
                                <option value="13">Affiliate Leads</option>
                                <option value="14">All Wallets</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!--// From date -->
                <div class="col-md-2">
                    <div class="form-group float-left">
                        <div class="input-group date datetime" id="date_from" data-target-input="nearest">
                            <input class="form-control datetimepicker-input" type="text" name="date_from" id="date_from" value="Select Date From" placeholder="Date From" data-target="#date_from">
                            <div class="input-group-append" data-target="#date_from" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--// To date -->
                <div class="col-md-2">
                    <div class="form-group float-left">
                        <div class="input-group date datetime" id="date_to" data-target-input="nearest">
                            <input class="form-control datetimepicker-input" type="text" name="date_to" id="date_to" value="Select Date To" placeholder="Date To" data-target="#date_to">
                            <div class="input-group-append" data-target="#date_to" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--// Button -->
                <div class="col-md-3">
                    <div class="form-group float-right">
                        {{ html()->submit($text = icon('fas fa-cog')." Refresh")->class('btn btn-outline-success data_request') }}

                        <x-buttons.create route='{{ route("backend.$module_name.create") }}' title="{{__('Create')}} {{ ucwords(Str::singular($module_name)) }}" />
                        <div class="btn-group float-right" role="group" style="margin-left: 3px;" aria-label="Toolbar button groups">
                            <div class="btn-group" role="group">
                                <button id="btnGroupToolbar" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-cog"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupToolbar">
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-eye-slash"></i> View Trash
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/.row-->
        {{ html()->closeModelForm() }}

        <hr style="margin-bottom: -1px;">
        <div class="small text-muted">
            Multiple updates are available for Wallets only.<br>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="float-left">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="wallet_id">Wallet Action</label>
                        </div>
                        <select class="custom-select" id="wallet_id">
                            <option selected>select action</option>
                            <option value="0"> Deposit </option>
                            <option value="1"> Withdrawal </option>
                            <option value="2"> Charge </option>
                            <option value="3"> Refund </option>
                            <option value="4">Bonus (Pending) </option>
                            <option value="5">Bonus (Risk Free Trade)</option>
                            <option value="6">Bonus (Smart Bonus)</option>
                            <option value="7">Bonus (Profit Bonus)</option>
                            <option value="8">Bonus (Balance Bonus)</option>
                            <option value="9">Bonus (Sticky Bonus)</option>
                            <option value="10">Bonus (No Deposit Bonus)</option>
                            <option value="11">Transfer In</option>
                            <option value="12">Transfer Out</option>
                        </select>
                    </div>
                </div>
                <!--// Match Dep. -->
                <div class="float-right">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <input type="checkbox" aria-label="input" id="deposit_id">
                            </div>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Match Dep.</span>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-sm-12 col-md-6">

                <div class="float-left">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text">ZAR</span>
                        </div>
                        <input type="text" class="form-control" id="bonus_id" aria-label="Amount">
                        <div class="input-group-append">
                            <span class="input-group-text">.00</span>
                        </div>
                    </div>
                </div>

                <div class="float-right">
                    {{ html()->submit($text = icon('fas fa-save')." Update")->class('btn btn-outline-danger update_all')->id('update_all') }}
                </div>
            </div>

        </div>

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="datatable" class="table table-hover">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>{{ __('labels.backend.traders.fields.userID') }}</th>
                                <th>Dep. Date</th>
                                <th>{{ __('labels.backend.traders.fields.firstName') }}</th>
                                <th>{{ __('labels.backend.traders.fields.lastName') }}</th>
                                <th>Currency</th>
                                <th>isDepositor</th>
                                <th>saleStatus</th>
                                <th class="text-center">
                                    <input type="checkbox" id="select_all" />
                                </th>
                                <th class="text-left">Select Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!--// Buttons bottom here -->

    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-7">
                <div class="float-left">

                </div>
            </div>
            <div class="col-5">
                <div class="float-right">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push ('after-styles')
<!--// DataTables Core and Extensions -->
<link rel="stylesheet" href="{{ asset('vendor/datatable/datatables.min.css') }}">
@endpush

@push ('after-scripts')
<!--// DataTables Core and Extensions -->
<script type="text/javascript" src="{{ asset('vendor/datatable/datatables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/datatable/datatables.select.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#datatable').DataTable({
        "language": {
            "search": "Search Data: "
        },
        processing: true,
        serverSide: true,
        autoWidth: false,
        responsive: true,
        ajax: '{{ route("backend.$module_name.index_data") }}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'userID', name: 'userID'},
            {data: 'firstDepositDate', name: 'firstDepositDate'},
            {data: 'firstName', name: 'firstName'},
            {data: 'lastName', name: 'lastName'},
            {data: 'currency', name: 'currency'},
            {data: 'isDepositor', name: 'isDepositor'},
            {data: 'saleStatus', name: 'saleStatus'},
            {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});
</script>

<!-- Date Time Picker & Moment Js-->
<script type="text/javascript" src="{{ asset('vendor/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap-4-datetime-picker/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script type="text/javascript">
$(function() {

    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    $('#date_to').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
            time: 'far fa-clock',
            date: 'far fa-calendar-alt',
            up: 'fas fa-arrow-up',
            down: 'fas fa-arrow-down',
            previous: 'fas fa-chevron-left',
            next: 'fas fa-chevron-right',
            today: 'far fa-calendar-check',
            clear: 'far fa-trash-alt',
            close: 'fas fa-times'
        }
    });

    $('#date_from').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
            time: 'far fa-clock',
            date: 'far fa-calendar-alt',
            up: 'fas fa-arrow-up',
            down: 'fas fa-arrow-down',
            previous: 'fas fa-chevron-left',
            next: 'fas fa-chevron-right',
            today: 'far fa-calendar-check',
            clear: 'far fa-trash-alt',
            close: 'fas fa-times'
        }
    });

    /** @var dataTable */
    var dataTable = $('#datatable').DataTable();

    /** Refresh search vars */
    var searchId;
    $('#search_id').on('change', function() {
        searchId = $( this ).val();

        $('#crm_id option').attr('disabled', true);
        $('.data_request').removeClass('btn-outline-success');
        $('.data_request').addClass('btn-success');

    });
    /** Refresh event */
     $('#data_request').click(function(e) {
         e.preventDefault();
         e.stopPropagation();
         console.log('search item: ' + searchId);
         dataTable.ajax.reload( null, false );

         $('input:checkbox:checked').prop('checked', false);
    });

     /** Mute currency input */
    $('#deposit_id').change(function(){
        if ($('#deposit_id').is(':checked') == true){
            $('#bonus_id').val('0.00').prop('disabled', true);
            console.log('checked');
        } else {
            $('#bonus_id').val('').prop('disabled', false);
            console.log('unchecked');
        }
    });

    /** Update All */
    $('body').on('click', '.update_all', function(eventAll) {
        eventAll.preventDefault();
        var rowIds = dataTable.rows( function ( idx, data, node ) {
            return $(node).find('.dt-checkbox:input[type="checkbox"]').prop('checked');
        }).data().toArray();

        if($('input[type="checkbox"]').prop('checked')) {
            $.ajax({
                type: 'GET',
                url: '{{ route("backend.$module_name.update_all") }}',
                data: {rowIds: rowIds},
                dataType: 'json',
                success: function(res){
                    console.log(res);
                    dataTable.ajax.reload( null, false );
                }
            });

        } else {
            $('.update_all').off('click.disable');
        }

    });

    /** Check all */
    $('#select_all').click(function() {
        if($(this).hasClass('checkedAll')) {
            $('input').prop('checked', false);
            $(this).removeClass('checkedAll');
            $('.update_all').removeClass('btn-danger');
            $('.update_all').addClass('btn-outline-danger');

        } else {
            $('input').prop('checked', true);
            $(this).addClass('checkedAll');
            $('.update_all').removeClass('btn-outline-danger');
            $('.update_all').addClass('btn-danger');
        }
    });

    /** Numbers only */
    $('#bonus_id').bind('keyup paste', function(){
        this.value = this.value.replace(/[^0-9]/g, '');
    });

});
</script>
@endpush
