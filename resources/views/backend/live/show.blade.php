@extends ('backend.layouts.app')

@section('title') {{ $module_action }} {{ $module_title }} @endsection

@section('breadcrumbs')
<x-backend-breadcrumbs>
    <x-backend-breadcrumb-item route='{{route("backend.$module_name.index")}}' icon='{{ $module_icon }}' >
        {{ $module_title }}
    </x-backend-breadcrumb-item>

    <x-backend-breadcrumb-item type="active">{{ $module_action }}</x-backend-breadcrumb-item>
</x-backend-breadcrumbs>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title mb-0">
                    <i class="{{$module_icon}}"></i> Live Traders Profile
                    <small class="text-muted">{{ __('labels.backend.traders.show.action') }} </small>
                </h4>
                <div class="small text-muted">
                    {{ __('labels.backend.traders.index.sub-title') }}
                </div>
            </div>
            <!--/.col-->
            <div class="col-4">
                <div class="float-right">
                    <a href="{{ route("backend.live.index") }}" class="btn btn-primary mt-1 btn-sm" data-toggle="tooltip" title="List"><i class="fas fa-list"></i> Back to List</a>
                    <a href="#" class="btn btn-primary mt-1 btn-sm" data-toggle="tooltip" title="Profile"><i class="fas fa-user"></i> Show Trader Profile</a>
                    <a href="#" class="btn btn-primary mt-1 btn-sm" data-toggle="tooltip" title="Edit {{ Str::singular($module_name) }} "><i class="fas fa-wrench"></i> Edit Profile</a>
                </div>
            </div>
            <!--/.col-->
        </div>
        <!--/.row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tr>
                            <th>Portfolio</th>
                            <td><img src="http://127.0.0.1:8000/img/default-avatar.jpg" class="user-profile-image img-fluid img-thumbnail" style="max-height:200px; max-width:200px;" /></td>
                        </tr>

                        <tr>
                            <th>First Name</th>
                            <td>{{ $trader->firstName }}</td>
                        </tr>
                        <tr>
                            <th>Last Name</th>
                            <td>{{ $trader->lastName }}</td>
                        </tr>

                        <tr>
                            <th>{{ __('labels.backend.traders.fields.email') }}</th>
                            <td> &nbsp; </td>
                        </tr>

                        <tr>
                            <th>{{ __('labels.backend.traders.fields.mobile') }}</th>
                            <td>&nbsp;</td>
                        </tr>

                        <tr>
                            <th>Providers</th>
                            <td>
                                <ul class="list-unstyled">
                                    @foreach ($trader->providers as $provider)
                                    <li>
                                        <i class="fab fa-{{ $provider->provider }}"></i> {{ label_case($provider->provider) }}
                                    </li>
                                    @endforeach
                                </ul>
                            </td>
                        </tr>

                        <tr>
                            <th>Bonus Status</th>
                            <td>{!! $trader->status_label !!}</td>
                        </tr>

                        <tr>
                            <th>Trade Volume</th>
                            <td>
                                <ul>
                                    <li>bla</li>
                                </ul>
                            </td>

                        </tr>
                        <tr>
                            <th>Last Deposit</th>
                            <td>
                                <ul>
                                    <li> bla </li>
                                </ul>
                            </td>
                        </tr>

                        <tr>
                            <th>{{ __('labels.backend.traders.fields.created_at') }}</th>
                            <td>{{ $trader->created_at }}<br><small>({{ $trader->updated_at }})</small></td>
                        </tr>

                        <tr>
                            <th>{{ __('labels.backend.traders.fields.updated_at') }}</th>
                            <td>{{ $trader->updated_at }}<br/><small>({{ $trader->created_at }})</small></td>
                        </tr>

                    </table>
                </div><!--/table-responsive-->

                <hr>

                @if ($trader->status != 2)
                <a href="#" class="btn btn-danger mt-1" data-method="PATCH" data-token="{{csrf_token()}}" data-toggle="tooltip" title="{{__('labels.backend.block')}}" data-confirm="Are you sure?"><i class="fas fa-ban"></i> Block</a>
                @endif
                @if ($trader->status == 2)
                <a href="#" class="btn btn-info mt-1" data-method="PATCH" data-token="{{csrf_token()}}" data-toggle="tooltip" title="{{__('labels.backend.unblock')}}" data-confirm="Are you sure?"><i class="fas fa-check"></i> Unblock</a>
                @endif
                <a href="#" class="btn btn-danger mt-1" data-method="DELETE" data-token="{{csrf_token()}}" data-toggle="tooltip" title="{{__('labels.backend.delete')}}" data-confirm="Are you sure?"><i class="fas fa-trash-alt"></i> Delete</a>
            </div><!--/col-->

            <div class="col">
                <h4>
                    Trade Activity
                </h4>
                <div class="table-responsive">
                    <table class="table table-responsive-sm table-hover table-bordered">
                        <thead>
                            <tr>
                                <th scope="col">
                                    <strong>
                                        Name
                                    </strong>
                                </th>
                                <th scope="col">
                                    <strong>
                                        Value
                                    </strong>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <strong>
                                        Label
                                    </strong>
                                </td>
                                <td>
                                    Column name
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        Label
                                    </strong>
                                </td>
                                <td>
                                    Column name
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>
                                        Label
                                    </strong>
                                </td>
                                <td>
                                    Column name
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--/.row-->
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    Updated: {{$trader->updated_at}},
                    Created at: {{$trader->created_at}}
                </small>
            </div>
        </div>
    </div>
</div>
@endsection
