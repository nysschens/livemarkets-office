<div class="text-left">
    <a href='{{ route("backend.$module_name.show", $data->id) }}' class="btn btn-success btn-sm mt-1" data-toggle="popover" title="{{__('labels.backend.show')}}"><i class="fas fa-desktop"></i></a>
    <a href="#" data-id="{{$data->userID}}" data-toggle="popover" title="Edit Trader" class="btn btn-primary btn-sm mt-1"><i class="fas fa-wrench"></i></a>
    <a href="#" class="btn btn-info btn-sm mt-1" data-toggle="popover" title="@lang('Send confirmation email')"><i class="fas fa-envelope"></i></a>
</div>
