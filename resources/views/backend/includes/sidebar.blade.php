<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand-new">
        <a href="{{ route('backend.dashboard') }}">
            <img class="c-sidebar-brand-full" src="/img/brand/coreui-base-white.svg" height="80" alt="coreui-base-white">
        </a>
    </div>

    {!! $admin_sidebar->asUl( ['class' => 'c-sidebar-nav'], ['class' => 'c-sidebar-nav-dropdown-items'] ) !!}

    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
</div>
