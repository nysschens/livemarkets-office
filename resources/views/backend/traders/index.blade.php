@extends ('backend.layouts.app')

@section('title') {{ $module_action }} {{ $module_title }} @endsection

@section('breadcrumbs')
<x-backend-breadcrumbs>
    <x-backend-breadcrumb-item type="active" icon='{{ $module_icon }}'>{{ $module_title }}</x-backend-breadcrumb-item>
</x-backend-breadcrumbs>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">

            <div class="col-8">
                <h4 class="card-title mb-0">
                    <i class="{{$module_icon}}"></i> <p>Active Traders</p>
                    <small class="text-muted">{{ __('labels.backend.traders.index.action') }} </small>
                </h4>
                <div class="small text-muted">
                    {{ __('labels.backend.traders.index.sub-title') }}
                </div>
            </div>

            <div class="col-4">
                <div class="float-right">
                    <x-buttons.create route='{{ route("backend.$module_name.create") }}' title="{{__('Create')}} {{ ucwords(Str::singular($module_name)) }}"/>

                    <div class="btn-group" role="group" aria-label="Toolbar button groups">
                        <div class="btn-group" role="group">
                            <button id="btnGroupToolbar" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-cog"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="btnGroupToolbar">
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-eye-slash"></i> View Trash
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--/.col-->
        </div>
        <!--/.row-->

        <div class="row mt-4">
            <div class="col">
                <table class="table table-hover table-responsive-sm">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>{{ __('labels.backend.traders.fields.userID') }}</th>
                            <th>First Deposit</th>
                            <th>{{ __('labels.backend.traders.fields.firstName') }}</th>
                            <th>{{ __('labels.backend.traders.fields.lastName') }}</th>
                            <th>{{ __('labels.backend.traders.fields.currency') }}</th>
                            <th>isDepositor</th>
                            <th>SaleStatus</th>
                            <th class="text-right">
                                <input type="checkbox" id="select_all" />
                            </th>
                            <th class="text-left">Select Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($traders as $trader)
                        <tr>
                            <td>{{ $trader->userID }}</td>
                            <td>{{ $trader->firstName }}</td>
                            <td>{{ $trader->lastName }}</td>
                            <td>
                                {!! $trader->saleStatus !!}
                            </td>
                            <td>
                                @if($trader->getRoleNames()->count() > 0)
                                    <ul>
                                        @foreach ($trader->getRoleNames() as $role)
                                        <li>{{ ucwords($role) }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </td>
                            <td>
                                @if($trader->getAllPermissions()->count() > 0)
                                    <ul>
                                        @foreach ($trader->getDirectPermissions() as $permission)
                                        <li>{{ $permission->name }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </td>
                            <td>
                                <ul class="list-unstyled">
                                    @foreach ($trader->providers as $provider)
                                    <li>
                                        <i class="fab fa-{{ $provider->provider }}"></i> {{ label_case($provider->provider) }}
                                    </li>
                                    @endforeach
                                </ul>
                            </td>
                            <td>{{ $trader->updated_at->diffForHumans() }}</td>
                            <td class="text-right">
                                <a href="#" class="btn btn-danger btn-sm mt-1" data-toggle="tooltip" title="Live Data"><i class="fas fa-desktop"></i></a>
                                <a href="{{route('backend.traders.show', $trader)}}" class="btn btn-success btn-sm mt-1" data-toggle="tooltip" title="{{__('labels.backend.show')}}"><i class="fas fa-desktop"></i></a>
                                <a href="{{route('backend.traders.edit', $trader)}}" class="btn btn-primary btn-sm mt-1" data-toggle="tooltip" title="{{__('labels.backend.edit')}}"><i class="fas fa-wrench"></i></a>
                                <a href="{{route('backend.traders.changePassword', $trader)}}" class="btn btn-info btn-sm mt-1" data-toggle="tooltip" title="{{__('labels.backend.changePassword')}}"><i class="fas fa-key"></i></a>
                                @if ($trader->status != 2)
                                <a href="{{route('backend.traders.block', $trader)}}" class="btn btn-danger btn-sm mt-1" data-method="PATCH" data-token="{{csrf_token()}}" data-toggle="tooltip" title="{{__('labels.backend.block')}}" data-confirm="Are you sure?"><i class="fas fa-ban"></i></a>
                                @endif
                                @if ($trader->status == 2)
                                <a href="{{route('backend.traders.unblock', $trader)}}" class="btn btn-info btn-sm mt-1" data-method="PATCH" data-token="{{csrf_token()}}" data-toggle="tooltip" title="{{__('labels.backend.unblock')}}" data-confirm="Are you sure?"><i class="fas fa-check"></i></a>
                                @endif
                                <a href="{{route('backend.traders.destroy', $trader)}}" class="btn btn-danger btn-sm mt-1" data-method="DELETE" data-token="{{csrf_token()}}" data-toggle="tooltip" title="{{__('labels.backend.delete')}}" data-confirm="Are you sure?"><i class="fas fa-trash-alt"></i></a>
                                @if ($trader->email_verified_at == null)
                                <a href="{{route('backend.traders.emailConfirmationResend', $trader->id)}}" class="btn btn-primary btn-sm mt-1" data-toggle="tooltip" title="Send Confirmation Email"><i class="fas fa-envelope"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $traders->total() !!} {{ __('labels.backend.total') }}
                </div>
            </div>
            <div class="col-5">
                <div class="float-right">
                    {!! $traders->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
