@extends ('backend.layouts.app')

@section('title') {{ $module_action }} {{ $module_title }} @endsection

@section('breadcrumbs')
<x-backend-breadcrumbs>
    <x-backend-breadcrumb-item route='{{route("backend.$module_name.index")}}' icon='{{ $module_icon }}' >
        {{ $module_title }}
    </x-backend-breadcrumb-item>

    <x-backend-breadcrumb-item type="active">{{ $module_action }}</x-backend-breadcrumb-item>
</x-backend-breadcrumbs>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title mb-0">
                    <i class="{{$module_icon}}"></i> Trader Profile
                    <small class="text-muted">{{ __('labels.backend.traders.show.action') }} </small>
                </h4>
                <div class="small text-muted">
                    {{ __('labels.backend.traders.index.sub-title') }}
                </div>
            </div>
            <!--/.col-->
            <div class="col-4">
                <div class="float-right">
                    <a href="{{ route("backend.traders.index") }}" class="btn btn-primary mt-1 btn-sm" data-toggle="tooltip" title="List"><i class="fas fa-list"></i> Back to List</a>
                    <a href="#" class="btn btn-primary mt-1 btn-sm" data-toggle="tooltip" title="Edit {{ Str::singular($module_name) }} "><i class="fas fa-wrench"></i> Edit Profile</a>
                </div>
            </div>
            <!--/.col-->
        </div>
        <!--/.row -->
        <br />

        <div class="row">
            <div class="col-8">

                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a href="#dashboard" class="nav-link active" data-toggle="tab">Dashboard</a>
                    </li>
                    <li class="nav-item">
                        <a href="#crm" class="nav-link" data-toggle="tab">CRM</a>
                    </li>
                    <li class="nav-item">
                        <a href="#banking" class="nav-link" data-toggle="tab">Banking</a>
                    </li>
                    <li class="nav-item">
                        <a href="#trades" class="nav-link" data-toggle="tab">Trades</a>
                    </li>
                    <li class="nav-item">
                        <a href="#practice" class="nav-link" data-toggle="tab">Practice</a>
                    </li>
                    <li class="nav-item">
                        <a href="#risk" class="nav-link" data-toggle="tab">Risk</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade show active" id="dashboard">
                        <h4 class="mt-2">Dashboard content</h4>

                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <img src="http://127.0.0.1:8000/img/default-avatar.jpg" class="user-profile-image img-fluid img-thumbnail" style="max-height:200px; max-width:200px;" />
                                    </th>
                                </tr>

                                <tr>
                                    <th>First Name</th>
                                    <td>{{ $trader->firstName }}</td>
                                </tr>
                                <tr>
                                    <th>Last Name</th>
                                    <td>{{ $trader->lastName }}</td>
                                </tr>

                                <tr>
                                    <th>{{ __('labels.backend.traders.fields.email') }}</th>
                                    <td> &nbsp; </td>
                                </tr>

                                <tr>
                                    <th>{{ __('labels.backend.traders.fields.mobile') }}</th>
                                    <td>&nbsp;</td>
                                </tr>

                                <tr>
                                    <th>Providers</th>
                                    <td>
                                        <ul class="list-unstyled">
                                            @foreach ($trader->providers as $provider)
                                                <li>
                                                    <i class="fab fa-{{ $provider->provider }}"></i> {{ label_case($provider->provider) }}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Bonus Status</th>
                                    <td>{!! $trader->status_label !!}</td>
                                </tr>

                                <tr>
                                    <th>Trade Volume</th>
                                    <td>
                                        <ul>
                                            <li>bla</li>
                                        </ul>
                                    </td>

                                </tr>
                                <tr>
                                    <th>Last Deposit</th>
                                    <td>
                                        <ul>
                                            <li> bla </li>
                                        </ul>
                                    </td>
                                </tr>

                                <tr>
                                    <th>{{ __('labels.backend.traders.fields.created_at') }}</th>
                                    <td>{{ $trader->created_at }}<br><small>({{ $trader->updated_at }})</small></td>
                                </tr>

                                <tr>
                                    <th>{{ __('labels.backend.traders.fields.updated_at') }}</th>
                                    <td>{{ $trader->updated_at }}<br/><small>({{ $trader->created_at }})</small></td>
                                </tr>

                            </table>
                        </div>
                    </div>
                    <!--// dashboard -->

                    <div class="tab-pane fade" id="crm">
                        <h4 class="mt-2">CRM Content</h4>
                        <p>
                            Trade information and related content will soon be available.
                            Please nibh urna, ornare id gravida ut, mollis a magna.
                            Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut.
                            Nam hendrerit bibendum turpis, sed molestie mi fermentum id.
                            Aenean volutpat velit sem. Sed consequat ante in rutrum convallis.
                            Nunc facilisis leo at faucibus adipiscing.
                        </p>

                        <div class="col-8">
                            <div class="table-responsive">
                                <table class="table table-responsive-sm table-hover table-borderless">
                                    <tbody>
                                    <tr>
                                        <td> Type: </td>
                                        <td>
                                            <div class="form-group">
                                                {!! Form::input('textarea', 'Select', null, ['class' => 'form-control']) !!}
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Contact On: </td>
                                        <td>
                                            {!! Form::input('textarea', 'Contact On', null, ['class' => 'form-control']) !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Compliance: </td>
                                        <td>
                                            {!! Form::input('textarea', 'Compliance', null, ['class' => 'form-control']) !!}
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="banking">
                        <br />
                        <p>
                            Banking profile and related information available here.
                        </p>

                        <h5>Wallet Action</h5>
                        <div class="col-8">
                            <div class="table-responsive">
                                <table class="table table-responsive-sm table-hover table-bordered">
                                    <tbody>
                                        <?php
                                        $field_name = 'action';
                                        $field_lable = label_case($field_name);
                                        $field_placeholder = "-- Select an option --";
                                        $required = "optional";
                                        $select_options = [
                                            '1' => 'Deposit',
                                            '2' => 'Withdrawal',
                                            '3' => 'Refund',
                                            '4' => 'Bonus (Profit Bonus)',
                                            '5' => 'Bonus (Balance Bonus)',
                                            '6' => 'Transfer In'
                                        ];
                                        ?>
                                        <tr>
                                            <td>
                                                {{ html()->label($field_lable, $field_name) }} {!! fielf_required($required) !!}
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    {{ html()->select($field_name, $select_options)->placeholder($field_placeholder)->class('form-control select2')->attributes(["$required"]) }}
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> Amount: </td>
                                            <td>
                                                <div class="input-group mb-2">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Curency</span>
                                                    </div>
                                                    <input type="text" class="form-control" id="bonus_id" aria-label="Amount">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">.00</span>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> Notes: </td>
                                            <td>
                                                {!! Form::input('textarea', 'notes', null, ['class' => 'form-control']) !!}
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <h6><b>Legend</b></h6>
                        <p>
                            Place holder copy nec erat eu nulla rhoncus fringilla ut non neque.
                            Vivamus nibh urna, ornare id gravida ut, mollis a magna.
                            Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut.
                            Nam hendrerit bibendum turpis, sed molestie mi fermentum id.
                            Aenean volutpat velit sem. Sed consequat ante in rutrum convallis.
                            Nunc facilisis leo at faucibus adipiscing.
                        </p>

                    </div>
                    <div class="tab-pane fade" id="trades">
                        <h4 class="mt-2">Trades</h4>
                        <p>
                            Trade information and related content will soon be available.
                            Please nibh urna, ornare id gravida ut, mollis a magna.
                            Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut.
                            Nam hendrerit bibendum turpis, sed molestie mi fermentum id.
                            Aenean volutpat velit sem. Sed consequat ante in rutrum convallis.
                            Nunc facilisis leo at faucibus adipiscing.
                        </p>
                    </div>
                    <div class="tab-pane fade" id="practice">
                        <h4 class="mt-2">Practice</h4>
                        <p>
                            Practice information and related content will soon be available.
                            Please nibh urna, ornare id gravida ut, mollis a magna.
                            Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut.
                            Nam hendrerit bibendum turpis, sed molestie mi fermentum id.
                            Aenean volutpat velit sem. Sed consequat ante in rutrum convallis.
                            Nunc facilisis leo at faucibus adipiscing.
                        </p>
                    </div>
                    <div class="tab-pane fade" id="risk">
                        <h4 class="mt-2">Risk</h4>
                        <p>
                            Risk information placeholder and related content available here.
                            Please nibh urna, ornare id gravida ut, mollis a magna.
                            Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut.
                        </p>
                    </div>


                </div>

            </div>
        </div>

        <div class="row mt-4 mb-4">
            <div class="col-8">

                <hr>
                @if ($trader->status != 2)
                <a href="#" class="btn btn-success mt-1" data-method="SAVE" data-token="{{csrf_token()}}" data-toggle="tooltip" title="{{__('labels.backend.save')}}" data-confirm="Are you sure?"><i class="fas fa-check"></i> Update</a>
                @endif
                @if ($trader->status == 2)
                <a href="#" class="btn btn-info mt-1" data-method="PATCH" data-token="{{csrf_token()}}" data-toggle="tooltip" title="{{__('labels.backend.unblock')}}" data-confirm="Are you sure?"><i class="fas fa-check"></i> Unblock</a>
                @endif
                <a href="#" class="btn btn-outline-primary mt-1" data-token="{{csrf_token()}}" data-toggle="tooltip" title="Ignore" data-confirm="ignore?"><i class="fas fa-question-circle"></i> Ignore</a>
            </div>
            <!--/col-->

        </div>
        <!--/.row-->
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    Updated: {{$trader->updated_at}},
                    Created at: {{$trader->created_at}}
                </small>
            </div>
        </div>
    </div>
</div>
@endsection
