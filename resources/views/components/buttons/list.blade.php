@props(["route"=>"", "icon"=>"fas fa-users-cog", "title", "small"=>"", "class"=>""])

@if($route)
<a href='{{$route}}'
 class='btn btn-danger {{($small=='true')? 'btn-sm' : ''}} {{$class}}'
 id="refreshData"
 data-toggle="popover"
 title="{{ $title }}">
 <i class="{{$icon}}"></i>
 {{ $slot }}
</a>
@else
<button type="submit"
 class='btn btn-danger {{($small=='true')? 'btn-sm' : ''}} {{$class}}'
 id="refreshData"
 data-toggle="popover"
 title="{{ $title }}">
 <i class="{{$icon}}"></i>
 {{ $slot }}
</button>
@endif
