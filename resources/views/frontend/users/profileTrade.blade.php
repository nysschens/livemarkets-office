@extends('frontend.layouts.trade')

@section('title') {{$$module_name_singular->name}}'s Trade Profile @endsection

@section('content')

    <h2 class="text-dark">Trade Profile for {{$$module_name_singular->name}}</h2>
    <h3 class="display-2 mb-4">
        Profile Id: {{$$module_name_singular->id}}

        @auth
            @if(auth()->user()->id == $$module_name_singular->id)
                <button>
                    <a href="{{ route('frontend.users.profile', $$module_name_singular->id) }}" class="btn btn-success btn-lg">Close Trading</a>
                </button>
            @endif
        @endauth
    </h3>

<div id="bpwidgets"></div>
<script type="text/javascript">
    bpApp({
        apiHost: 'https://bpw.livemarkets.co.za',
        themeSet: 'default',
        lang: 'en',
        state: 'wowMain.register',
        redirectSuccess: 'https://www.livemarkets.co.za/deposit',
        redirectFailure: 'https://livemarkets.co.za/',
    }).render( '#bpwidgets');
</script>
