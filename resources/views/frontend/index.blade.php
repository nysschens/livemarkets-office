@extends('frontend.layouts.app')

@section('title') {{app_name()}} @endsection

@section('content')

<section class="section-header text-white pb-7 pb-lg-11">
    <div class="container">
        <div class="row">
            <div class="col-6 col-md-6 text-left">

                @include('frontend.includes.messages')

            </div>
            <div class="col-6 col-md-6">

            </div>
        </div>
    </div>
</section>

<section class="section section-ld">
    <div class="container">
        <div class="row">


        </div>
    </div>
</section>
@endsection
